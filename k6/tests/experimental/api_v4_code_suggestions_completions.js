/*global __ENV : true  */
/*
@endpoint: `POST /code_suggestions/completions`
@description: [Generate code completions](https://docs.gitlab.com/ee/api/code_suggestions.html#generate-code-completions)
@gitlab_settings: { "instance_level_ai_beta_features_enabled": true }
@gpt_data_version: 1
*/

import http from "k6/http";
import { check } from "k6";
import { group } from "k6";
import { Rate } from "k6/metrics";
import {
  logError,
  getRpsThresholds,
  getTtfbThreshold,
} from "../../lib/gpt_k6_modules.js";

export let thresholds = {
  'ttfb': { 'latest': 500 },
};
export let rpsThresholds = getRpsThresholds()
export let ttfbThreshold = getTtfbThreshold(thresholds['ttfb'])
export let successRate = new Rate("successful_requests");
export let options = {
  thresholds: {
    successful_requests: [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    http_req_waiting: [`p(90)<${ttfbThreshold}`],
    http_reqs: [`count>=${rpsThresholds["count"]}`],
  },
};

export function setup() {
  console.log("");
  console.log(`RPS Threshold: ${rpsThresholds["mean"]}/s (${rpsThresholds["count"]})`);
  console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`);
  console.log(`Success Rate Threshold: ${parseFloat(__ENV.SUCCESS_RATE_THRESHOLD) * 100}%`);
}

export default function () {
  group("API - Code Suggestions - Completions", function () {
    let params = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${__ENV.ACCESS_TOKEN}`,
      },
    };
    let body = {
      intent: "completion",
      current_file: {
        file_name: "main.py",
        content_above_cursor: "def reverse_string(s):\n    return s[::-1]\ndef test_empty_input_string()",
        content_below_cursor: "",
      },
    };

    let res = http.post(
      `${__ENV.ENVIRONMENT_URL}/api/v4/code_suggestions/completions`,
      JSON.stringify(body),
      params
    );
    check(res, {
      "is status 200": (r) => r.status === 200,
    });
    check(res, {
      "verify response has choices": (r) => r.body.includes("choices"),
    });
    /200/.test(res.status)
      ? successRate.add(true)
      : (successRate.add(false), logError(res));
  });
}
